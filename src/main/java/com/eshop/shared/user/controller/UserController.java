package com.eshop.shared.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eshop.shared.Jwt.JwtTokenUtil;
import com.eshop.shared.security.service.CustomUserDetailsService;
import com.eshop.shared.util.APIResponse;
import com.eshop.shared.util.APIResponseUtility;
import com.eshop.shared.util.LoginRequest;

@RestController
public class UserController {

	@Autowired
	private AuthenticationManager authenticationmanager;	
	
	@Autowired
	private CustomUserDetailsService customuserdetailservice;
	
	@Autowired
	private JwtTokenUtil jwttokenutil;
	
	@Autowired
	private APIResponseUtility responseutility;
	
	
	@GetMapping("/token")
	public APIResponse generateToken(@RequestBody LoginRequest loginrequest) throws Exception {
		try {
			this.authenticationmanager.authenticate(new UsernamePasswordAuthenticationToken(loginrequest.getUsername(), loginrequest.getPassword()));
		} catch(UsernameNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Bad Credential");
			
		}
		UserDetails userdetails = customuserdetailservice.loadUserByUsername(loginrequest.getUsername()); 
		String token = this.jwttokenutil.generateToken(userdetails);
		APIResponse response = responseutility.SuccessResponse(token);
		return response;
		
	}

}
