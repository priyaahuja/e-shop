package com.eshop.shared.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "USER")
public class User {
	@Id
	private Integer Id;
	@Column(nullable = false, unique = true)
	private String username;
//	private String username;
	private String password;
	private String emailid;
	
	
	public User(Integer id, String username, String password, String emailid) {
		this.Id = id;
		this.username = username;
		this.password = password;
		this.emailid = emailid;
	}


	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

}
