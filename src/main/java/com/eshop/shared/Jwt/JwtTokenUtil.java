package com.eshop.shared.Jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;


@Service
public class JwtTokenUtil {
	private String SECRET_KEY = "secret";

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder()
        		.setClaims(claims)
        		.setSubject(subject)
        		.setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    
    @SuppressWarnings("unused")
	private String GenerateEncryptedToken(JwtClaims claims)throws InvalidJwtException {
    	String encryptedJwt = "";
    	try {
    	//Generation of content encryption key
    	KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    	keyGen.init(256);
    	SecretKey contentEncryptKey = keyGen.generateKey();
    	JsonWebEncryption jwe = new JsonWebEncryption();
		//jwe.setKey(receipentPubKey);
		jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA_OAEP_256);
		jwe.setPayload(claims.toJson());
		jwe.setContentEncryptionKey(contentEncryptKey.getEncoded());
		jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
		SecureRandom iv = SecureRandom.getInstance("SHA1PRNG");
		jwe.setIv(iv.generateSeed(16));
		encryptedJwt = jwe.getCompactSerialization();
		System.out.println("Encrypted ::" + encryptedJwt);
    	} 
//    	catch (InvalidJwtException e) {
//    	  e.printStackTrace();
//		} 
    	catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}catch (JoseException e) {
			e.printStackTrace();
	   }catch (Exception e) {
			// TODO: handle exception
		}
		return encryptedJwt;
}
    
    private Boolean ValidateEncryptedToken(String encryptedJwt) {
    	 boolean Status= false; 
    	try {
    	JwtConsumer consumer = new JwtConsumerBuilder()
				.setExpectedAudience("Admins")
				.setExpectedIssuer("CA")
				.setRequireSubject()
//				.setDecryptionKey(ceKey.getPrivateKey())
				.setDisableRequireSignature()
				.build();
               JwtClaims receivedClaims = consumer.processToClaims(encryptedJwt);
               Status = true;
               System.out.println("SUCESS :: JWT Validation");
    	} catch (Exception e) {
    		e.printStackTrace();
			// TODO: handle exception
		}
    	return true;
    	
    }

}
