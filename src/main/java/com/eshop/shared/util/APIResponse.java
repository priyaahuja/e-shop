package com.eshop.shared.util;

import lombok.Data;

@Data
public class APIResponse {
	private String status;
	private String message;
	private Object data;
}
