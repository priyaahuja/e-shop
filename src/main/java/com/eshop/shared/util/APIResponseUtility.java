package com.eshop.shared.util;
import org.springframework.stereotype.Service;

import com.eshop.shared.common.Constant;

@Service
public class APIResponseUtility {
	
	public APIResponse SuccessResponse(Object data) {
		APIResponse apiresponse = new APIResponse();
		apiresponse.setStatus(Constant.SUCCESS_STATUS);
		apiresponse.setMessage(Constant.MESSAGE_SUCCESS);
		apiresponse.setData(data);
		return apiresponse;
	}
	
	public APIResponse FailResponse(Object data) {
		APIResponse apiresponse = new APIResponse();
		apiresponse.setStatus(Constant.FAIL_STATUS);
		apiresponse.setMessage(Constant.MESSAGE_FAIL);
		apiresponse.setData(data);
		return apiresponse;
	}

}
