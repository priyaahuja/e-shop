package com.eshop;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.eshop.shared.user.entity.User;
import com.eshop.shared.user.repository.UserRepository;

@SpringBootApplication
public class EShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(EShopApplication.class, args);
	}
	
//	@Autowired
//	private UserRepository userRepository;
//	
//	@PostConstruct
//	public void initUser()
//	{
//		List<User> users = Stream.of(
//				new User(101,"priyanka","priyanka@123","priyanka@gmail.com"),
//				new User(102,"sagar","sagar@123","sagar@gmail.com"),
//				new User(103,"shivam","shivam@123","shivam@gmail.com"),
//				new User(104,"ahuja","ahuja@123","ahuja@gmail.com")).collect(Collectors.toList());
//				userRepository.saveAll(users);
//		
//	}
	
//	List<Object> user =	(List<Object>) userRepository.findByUsername("priyanka");

}
